#!/usr/bin/env python3

import json
import os
import yaml

# Load the servers list from vars/main.yml
with open(os.path.join(os.path.dirname(__file__), '.', 'vars', 'main.yml')) as f:
    vars_data = yaml.safe_load(f)

servers = vars_data.get('servers', [])

# Generate the inventory in JSON format
inventory = {
    "all": {
        "hosts": servers
    }
}

print(json.dumps(inventory))

