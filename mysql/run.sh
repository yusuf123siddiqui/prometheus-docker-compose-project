#!/bin/bash
mysql -u root -p$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE gold;"
mysql -u root -p$MYSQL_ROOT_PASSWORD -e "USE gold;" -e "source /data/gold_rates_index.sql"
mysql -u root -p$MYSQL_ROOT_PASSWORD -e "USE gold;" -e "source /data/gold_rates_data.sql"
